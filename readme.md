# HTML version of Jack Cooc's resume which can be found at http://www.jackcooc.com/jackresume.pdf

I created this resume in HTML because my original resume was designed for me in Illustrator and I do not have an Illustrator license.

This HTML version allows me to utilize **JavaScript** for a more interactive experience.

Please note that I have not tested this for **mobile** or **cross-browser compatibility**.