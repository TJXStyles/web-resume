$(document).ready(function(){
  $(".divs div").each(function(e) {
     if (e != 0)
       $(this).hide();
  });

  $("#next").click(function() {
    var $visible = $(".divs div:visible");
    if ($visible.next().length != 0) {
      $visible.next().show().prev().hide();
    } else {
      $visible.hide();
      $(".divs div:first").show();
    }
    return false;
  });

  $("#prev").click(function() {
    var $visible = $(".divs div:visible");
    if ($visible.prev().length != 0) {
      $visible.prev().show().next().hide();
    } else {
      $visible.hide();
      $(".divs div:last").show();
    }
    return false;
  });

  $('#ref').on('click', function() {
    var $button = $('#ref');
    $('.references').slideToggle('slow', function() {
      $button.text($(this).is(':visible') ? "Hide" : "Show");
    });
  });
});